# .bash_profile

export LESS=-R
export LESS_TERMCAP_mb=$'\E[1;36m'
export LESS_TERMCAP_md=$'\E[1;36m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'
export LESS_TERMCAP_ue=$'\E[0m'

# Get the aliases and functions
[ -f $HOME/.bashrc ] && . $HOME/.bashrc
