#  _               _
# | |__   __ _ ___| |__  _ __ ___
# | '_ \ / _` / __| '_ \| '__/ __|
# | |_) | (_| \__ \ | | | | | (__
# |_.__/ \__,_|___/_| |_|_|  \___|

stty  -ixon #disable terminal freze.
shopt -s autocd #change dir by filename

export PATH=$PATH:/home/kannonbh/bin
export EDITOR=nvim

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias ll='ls -l --color=auto'
alias lh='ls -la'
alias pgm='cd /usr/local/src'
alias grep='grep --color=auto' # Color grep - highlight desired sequence.
alias mkdir='mkdir -pv' # Create parent directories if they don't exist
alias vim='nvim'
alias vd='vim $HOME/suckless/dwm/config.h'

# Arch Linux Aliases
alias pi='sudo pacman -S'
alias pu='sudo pacman -Syu'
alias pq='sudo pacman -Ss'
alias pr='sudo pacman -R'

# Debian Aliases
alias ai='sudo apt install'
alias ar='sudo apt remove'
alias au='sudo apt-get update && sudo apt-get upgrade'
alias as='sudo apt search'

# Terminal Spotify
alias ncspot='flatpak run io.github.hrkfdn.ncspot'

PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"
