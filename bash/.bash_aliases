alias ls='ls --color=auto'
alias ll='ls -l --color=auto'
alias lh='ls -la'
alias pgm='cd /usr/local/src'
alias grep='grep --color=auto' # Color grep - highlight desired sequence.
alias mkdir='mkdir -pv' # Create parent directories if they don't exist
alias vim='nvim'
alias pi='sudo pacman -S'
alias pu='sudo pacman -Syu'
alias pq='sudo pacman -Ss'
alias pr='sudo pacman -R'
