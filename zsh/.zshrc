# Lines configured by zsh-newuser-install

source $HOME/.bash_aliases

bindkey -v
setxkbmap -option caps:swapescape

export PATH=$PATH:/home/kannonbh/bin

HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd extendedglob nomatch
setopt COMPLETE_ALIASES
unsetopt beep
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/kannonbh/.zshrc'

PROMPT='%B%F{red}[%F{cyan}%n%F{green}@%F{yellow}%m %F{green}%~%F{red}]%F{white}$ %b'

autoload -Uz compinit
compinit

